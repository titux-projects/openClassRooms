<?php if(!isset($_POST['password']) || $_POST['password'] != 'kangourou'): ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Page protégée par mot de passe</title>
	</head>
	<body>
		<h1>Page protégée par mot de passe</h1>
		<?php if(isset($_POST['password'])): ?>
			<?php if(empty($_POST['password'])): ?>
				<p>Le mot de passe est vide !</p>
			<?php else: ?>
				<p>Mot de passe incorect !</p>
			<?php endif; ?>
		<?php endif; ?>
		<p>Veuillez entrer le mot de passe pour obtenir les codes d'accès au serveur de la NASA :</p>
		<form action="" method="post">
			<p>
				<label for="password">Mot de passe</label>
				<input type="password" id="password" name="password">
			</p>
			<p>
				<input type="submit" value="Valider">
			</p>
		</form>
	</body>
</html>
<?php else: ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Les codes secrets de la NASA</title>
	</head>
	<body>
		<ul>
			<li>Big brother</li>
			<li>Is watching you</li>
		</ul>
	</body>
</html>
<?php endif; ?>