<ul>
	<li><a href="simple_request.php">Lire dans la base avec une requête simple</a></li>
	<li><a href="prepared_request.php">Lire dans la base avec une requête préparée</a></li>
	<li><a href="write_request.php">Écrire des données dans la base</a></li>
	<li><a href="update_request.php">Modifier des données dans la base</a></li>
	<li><a href="delete_request.php">Supprimer des données dans la base</a></li>
</ul>