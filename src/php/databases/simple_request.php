<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Les bases de données</title>
	</head>
	<body>
		<h1>Les bases de données</h1>
		<h2>Lire des données : requêtes simples</h2>
		<p>
			Se connecter à la base de données avec php et pdo :<br>
			<?php
			try {
				$db = new PDO('mysql:host=db;dbname=openclassrooms', 'openclassrooms', 'openclassrooms');
				echo "Connection à la base de données OK";
			} catch(Exception $e) {
				die('Erreur : ' . $e->getMessage());
			}
			$query = "SELECT * FROM jeux_video WHERE console='PS2' OR console='PC' ORDER BY prix ASC LIMIT 0,5";
			$response = $db->query($query);
			?>
		</p>
		<p>
			On effectue la requête suivante :
			<pre>$query = 'SELECT * FROM jeux_video';</pre>
			qu'on envoie à la base de données via l'objet pdo :
			<pre>$response = $db->query('SELECT * FROM jeux_video');</pre>
			pdo nous renvoie la réponse qui peu contenir beaucoup d'informations qu'il faut parcourir.<br>On prend donc la réponse et on lis chaque ligne une à une avec :
			<pre>$data = $response->fetch();</pre>
			qu'il nous faut mettre dans une boucle while
			<pre>while($data = $response->fetch()) {</pre>
			pour pouvoir ensuite afficher les données de chaque ligne
			<pre>echo $data['nom'] $data['possesseur']...</pre>
		</p>
		<p>
			Voici le résultat :
		</p>
			<?php while($data = $response->fetch()): ?>
				<p>
					<strong>Jeu</strong> : <?= $data['nom']; ?><br>
					Le possesseur de ce jeu est : <?= $data['possesseur']; ?> et il le vend à <?= $data['prix']; ?> euros !<br>
					Ce jeux fonctionne sur <?= $data['console']; ?> et on peut y jouer à <?= $data['nbre_joueurs_max']; ?> au maximum.<br>
					<?= $data['possesseur']; ?> à laissé ces commentaires sur <?= $data['nom']; ?> : <em><?= $data['commentaires']; ?></em>
				</p>
			<?php endwhile; ?>
			<p>
				Pour terminer on ferme le curseur d'analyse des résultats, en gros le travail est fini
				<pre>$response->closeCursor();</pre>
			</p>
			<?php $response->closeCursor(); ?>
			<p>
				On peut également filtrer les résultats avec les mots clé sql WHERE, OR, AND, ORDER BY, LIMIT et bien d'autres : <a href="http://sql.sh" target="_blank">Voir la documentation sur sql.sh</a>
			</p>
			<?php require '_nav.php'; ?>
	</body>
</html>