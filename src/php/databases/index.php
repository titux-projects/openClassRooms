<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Les bases de données</title>
	</head>
	<body>
		<h1>Les bases de données</h1>
		<p>
			Se connecter à la base de données avec php et pdo :<br>
			<?php
			try {
				$db = new PDO('mysql:host=db;dbname=openclassrooms', 'openclassrooms', 'openclassrooms');
				echo "Connection à la base de données OK";
			} catch(Exception $e) {
				die('Erreur : ' . $e->getMessage());
			}
			?>
		</p>
		<p>
			On va effectuer plusieurs opérations sur la base de données :
			<?php require '_nav.php'; ?>
		</p>
	</body>
</html>