<?php
	try {
		$db = new PDO('mysql:host=db;dbname=openclassrooms', 'openclassrooms', 'openclassrooms');
		echo "Connection à la base de données OK";
	} catch(Exception $e) {
		die('Erreur : ' . $e->getMessage());
	}
	$possesseur = 'Michel';
	$prix_max = 20;
	$query = 'SELECT * FROM jeux_video WHERE possesseur=? AND prix <= ?';
	$request = $db->prepare($query);
	$request->execute([$possesseur, $prix_max]);

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Les bases de données</title>
	</head>
	<body>
		<h1>Les bases de données</h1>
		<h2>Lire des données : requêtes préparées</h2>
		<p>
			On veut donné au visiteur la possibilité de trouver les jeux d'une personne en particulier et d'un prix maximum.
		</p>
		<p>
			On prépare une requête qui va accepter des paramètres : possesseur et prix_max.<br>
			On utilise ici les marqueurs "?" pour construire la requête
			<pre>$req = $db->prepare("SELECT * FROM jeux_video WHERE possesseur=? AND prix <= ?");</pre>
			Ensuite on execute la requête en lui passant un tableau de paramètre
			<pre>$req->execute([$_GET['possesseur'], $_GET['prix_max']]);</pre>
		</p>
		<p>
			On peut ensuite faire une boucle while sur chaque ligne de résultat
			<ul>
				<?php while($data = $request->fetch()): ?>
					<li><?= $data['nom'] . ' (' . $data['prix'] . ' EUR)'; ?></li>
				<?php endwhile; ?>
			</ul>
		</p>
		<?php $request->closeCursor(); ?>
		<?php require '_nav.php'; ?>
	</body>
</html>