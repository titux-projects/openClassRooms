<?php	session_start(); ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Titre de la page</title>
	</head>
	<body>
		<h1>Re-hello <?= $_SESSION['prenom'] ?></h1>
		<p>
			Je me souviens de toi ! Tu t'appelles <?= $_SESSION['nom']; ?> <?= $_SESSION['prenom']; ?> !
		</p>
		<p>
			Et ton âge hummmmm... Tu as <?= $_SESSION['age']; ?> ans, c'est ça ?
		</p>
	</body>
</html>