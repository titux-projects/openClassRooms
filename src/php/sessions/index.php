<?php
	session_start();
	setcookie('pays', 'France', time() + 365*24*3600, null, null, false, true);

	$_SESSION['prenom'] = 'Guillaume';
	$_SESSION['nom'] = 'Lang';
	$_SESSION['age'] = 40;
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Titre de la page</title>
	</head>
	<body>
		<h1>Hello <?= $_SESSION['prenom'] ?></h1>
		<p>
			Tu es à l'accueil du site. Tu veux aller sur une autre page ?
			<ul>
				<li><a href="mapage.php">Lien vers mapage.php</a></li>
				<li><a href="monscript.php">Lien vers monscript.php</a></li>
				<li><a href="informations.php">Lien vers informations.php</a></li>
			</ul>
		</p>
	</body>
</html>