<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Bienvenue sur les exercices Php OpenClassRooms</title>
	</head>
	<body>
		<h1>Bienvenue sur les exercices Php OpenClassRooms</h1>
		<p>Voir les exercices suivants :</p>
		<ul>
			<li><a href="protected/">Page protégée par mot de passe</a></li>
			<li><a href="sessions/">Les sessions et les cookies</a></li>
			<li><a href="files/">Les fichiers</a></li>
			<li><a href="databases/">Les bases de données</a></li>
		</ul>
	</body>
</html>