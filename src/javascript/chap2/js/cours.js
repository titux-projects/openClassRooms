var a = 3.14;
console.log(a);

var b = 0;
b += 1;
b ++;
console.log(b);

var f = 100;
console.log("La valeur de f est : " + f);

var g = "cinq" * 2;
console.log(g);

var h = "5";
console.log(h + 1);
h = Number("5");
console.log(h + 1);