var a = 2;
a = a - 1;
// 1
console.log(a);
a++;
// 2
console.log(a);

var b = 8;
b += 2;
// 10
console.log(b);

var c = a + b * b;
// 2 + 10 * 10 = 120 -> non 102 car 10 * 10 + 2
console.log(c);
var d = a * b + b;
// 2 * 10 + 10 = 30
console.log(d);
var e = a * (b + b);
// 2 * (10 + 10) = 40
console.log(e);
var f = a * b / a;
// 2 * 10 / 2 = 10
console.log(f);
var g = b / a * a;
// 10 / 2 * 2 = 10
console.log(g);