<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Chapitre 2 : Les variables</title>
	</head>
	<body>
		<h1>Chapitre 2 : Les variables</h1>
		<p>Voir les exercices suivants :</p>
		<ul>
			<li><a href="html/cours.html">Les cours</a></li>
			<li><a href="html/bonjour.html">Bonjour</a></li>
			<li><a href="html/conversion.html">Conversion</a></li>
			<li><a href="html/permutation.html">Permutation</a></li>
			<li><a href="html/tva.html">Tva</a></li>
			<li><a href="html/valeurs.html">Valeurs</a></li>
		</ul>
	</body>
</html>