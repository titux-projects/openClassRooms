<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Chapitre 1 : Introduction</title>
	</head>
	<body>
		<h1>Chapitre 1 : Introduction</h1>
		<p>Voir les exercices suivants :</p>
		<ul>
			<li><a href="html/index.html">Cours</a></li>
			<li><a href="html/calculette.html">Calculette</a></li>
			<li><a href="html/presentation.html">Présentation</a></li>
			<li><a href="html/valeurs.html">Valeurs</a></li>
		</ul>
	</body>
</html>