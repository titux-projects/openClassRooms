<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Bienvenue sur les exercices Javascript OpenClassRooms</title>
	</head>
	<body>
		<h1>Bienvenue sur les exercices Javascript OpenClassRooms</h1>
		<p>Voir les exercices suivants :</p>
		<ul>
			<li><a href="chap1/">Introduction</a></li>
			<li><a href="chap2/">Les variables</a></li>
			<li><a href="chap3/">Les conditions</a></li>
		</ul>
	</body>
</html>