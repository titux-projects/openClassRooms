<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Bienvenue sur les exercices OpenClassRooms</title>
	</head>
	<body>
		<h1>Bienvenue sur les exercices OpenClassRooms</h1>
		<p>Voir les exercices des cours suivants :</p>
		<ul>
			<li><a href="php/protected/">Le langage php</a></li>
			<li><a href="html-css/">Le Html5 et le Css3</a></li>
			<li><a href="javascript/">Le javascript</a></li>
		</ul>
	</body>
</html>